# Django k8s Application

## Адрес приложения
[http://158.160.54.3:30003](http://158.160.54.3:30003)

## Список репозиториев
`https://gitlab.com/devops-skillfactory1/`

* infrastructure - terraform создание инфраструктуры
* configure - конфигурация ansible
* app - django docker-приложение
* k8s-django - k8s helm chart для django приложения
* k8s-prometheus - k8s helm chart для prometheus экспортеров
* screens - скриншоты работы приложения

Для сокрытия приватных данных используются переменные в CI/CD gitlab

Далее, подробнее о каждом репозитории

### infrastructure
[https://gitlab.com/devops-skillfactory1/infrastructure](https://gitlab.com/devops-skillfactory1/infrastructure)

Инфраструктура проекта находится в Яндекс.Облаке. 
Для её разворачивания необходимо:
* стянуть проект из гитлаба 
* указать настройки для подключения к Яндекс.Облаку
* установить terraform и запустить `terraform apply`

Terraform развернёт в облаке:
* k8s cluster с одной worker-нодой 
* виртуальную машину для управления кластером

### configure
[https://gitlab.com/devops-skillfactory1/configure](https://gitlab.com/devops-skillfactory1/configure)

**Перед началом работы необходимо установить на служебный сервер ansible**

**Все задачи выполнены с помощью ролей ansible для OS Ubuntu.**

Ansible playbooks для установки на служебный сервер необходимого ПО:
* docker
* gitlab_runner
* prometheus
* grafana
* alertmanager

### app

[https://gitlab.com/devops-skillfactory1/app](https://gitlab.com/devops-skillfactory1/app)

#### Django приложение
При добавлении тега в репозиторий в gitlab запускается pipeline, который:
* собирает новый образ приложения, ставит образу такой же тег, как и в репо
* отправляет новый образ в gitlab registry
* обновляет helm chart в k8s кластере из нового образа

### k8s-django

[https://gitlab.com/devops-skillfactory1/k8s-django](https://gitlab.com/devops-skillfactory1/k8s-django)

Helm chart для:
* приложения django
* PostgreSQL
* fluentbit

### k8s-prometheus

[https://gitlab.com/devops-skillfactory1/k8s-prometheus](https://gitlab.com/devops-skillfactory1/k8s-prometheus)

Helm Chart для экспортеров метрик prometheus из k8s кластера:
* node-exporter - для информации об узле в кластере
* blackbox-exporter - для приложения django

### screens

[https://gitlab.com/devops-skillfactory1/screens](https://gitlab.com/devops-skillfactory1/screens)

Скриншоты работы приложения
